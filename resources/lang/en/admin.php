<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Article' => 'Article',
    'sign_in' => 'Sign in to start your session',
    'sign_in_btn' => 'Sign in',
    'add_article' => 'Add Article',
    'edit_article' => 'Edit Article',
    'title' => 'Title',
    'images' => 'Images',
    'text' => 'Text',
    'enter' => 'Enter....',
    'submit' => 'Submit',
    'articles_table' => 'Articles Table',
    'bio' => 'Bio',
    'user' => 'User',
    'created_at' => 'Created at',
    'edit' => 'Edit',
    'delete' => 'Delete',


];
