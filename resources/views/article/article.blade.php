@include('article.header')
<body>

@include('article.navbar')

<div class="container">
    <section>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                    <ul class="bxslider">
                        @foreach($result['data']->images as $image)
                            <?php $path = 'images/medium_size/' . $image->name; ?>
                            <li><div class="slider-item"><img class="slider-article-img" src="{{asset($path)}}" title="Funky roots" /></div></li>
                        @endforeach
                    </ul>
                <article class="blog-post">
                    <div class="blog-post-image">
                        <a href="post.html"><img src="{{asset('Articles/images/750x500-5.jpg')}}" alt=""></a>
                    </div>
                    <div class="blog-post-body">
                        <h2><a href="post.html">{{$result['data']->title}}</a></h2>
                        <div class="post-meta"><span>by {{$result['data']->username}}</span>/<span><i class="fa fa-clock-o"></i>posted in {{$result['data']->created_at->format('Y-m-d')}}</span></div>
                        <div class="blog-post-text">
                            <?php echo $result['data']->text; ?>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>
</div><!-- /.container -->

@include('article.footer')