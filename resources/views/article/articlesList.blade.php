@include('article.header')
<body>

@include('article.navbar')

<div class="container">
    <header>
        <a href="{{route('article.index')}}"><h1 class="title">DownTown</h1></a>
    </header>
    <section class="main-slider">
        <ul class="bxslider">
            <li><div class="slider-item"><img class="slider-img" src="{{asset('Articles/images/slider1.jpeg')}}" title="Funky roots" /><h2><a href="{{route('article.index')}}" title="Vintage-Inspired Finds for Your Home">Today a reader, tomorrow a leader</a></h2></div></li>
            <li><div class="slider-item"><img class="slider-img" src="{{asset('Articles/images/slider2.jpeg')}}" title="Funky roots" /><h2><a href="{{route('article.index')}}" title="Vintage-Inspired Finds for Your Home">Reading one book is like eating one potato chip</a></h2></div></li>
            <li><div class="slider-item"><img class="slider-img" src="{{asset('Articles/images/slider3.jpeg')}}" title="Funky roots" /><h2><a href="{{route('article.index')}}" title="Vintage-Inspired Finds for Your Home">Think before you speak. Read before you think</a></h2></div></li>
        </ul>
    </section>
    <hr class="first-line">
    <section>
        <div class="row">
            <div class="col-md-12">
                @if($result['data'])
                    @foreach($result['data'] as $item)
                        <article class="blog-post">
                            <div class="blog-post-image">
                                @if($item->images)
                                    @if(sizeof($item->images))
                                        <a href="{{route('article.show', $item->id)}}">
                                            <?php $path = 'images/medium_size/' . $item->images[0]->name; ?>
                                            <img class="articles-img" src="{{asset($path)}}" alt="">
                                        </a>
                                    @endif
                                @endif
                            </div>
                            <div class="blog-post-body">
                                <h2><a href="{{route('article.show', $item->id)}}">{{$item->title}}</a></h2>
                                <div class="post-meta"><span>by {{$item->username}}</span>/<span><i class="fa fa-clock-o"></i>posted in {{$item->created_at->format('Y-m-d')}}</span></div>
                                <p>{{$item->bio}}</p>
                                <div class="read-more"><a href="{{route('article.show', $item->id)}}">Continue Reading</a></div>
                            </div>
                        </article>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
</div><!-- /.container -->

@include('article.footer')