<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <title>Renda - clean blog theme based on Bootstrap</title>
    <!-- Bootstrap core CSS -->
    <link href="{{asset('Articles/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Custom styles for this template -->
    <link href="{{asset('Articles/css/jquery.bxslider.css')}}" rel="stylesheet">
    <link href="{{asset('Articles/css/style.css')}}" rel="stylesheet">

    <style>
        .title{
            font-size: 50px;
        }
        .slider-img{
            max-height: 607px;
            width: 100%;
        }
        .slider-article-img{
            max-height: 400x;
            width: 100%;
        }
        .articles-img{
            max-height: 500px;
            width: 100%;
        }
        .first-line{
            margin-bottom: 100px;
        }
    </style>
</head>
