<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <!-- <a href="index3.html" class="brand-link">
        <span class="brand-text font-weight-light">Articles</span>
    </a> -->

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item menu-open">
                    <ul class="nav nav-treeview" id="sidebar">
                        <li class="nav-item" id="list">
                            <a href="{{route('admin_article.index')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Articles List</p>
                            </a>
                        </li>
                        <li class="nav-item" id="add">
                            <a href="{{route('admin_article.create')}}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Articles</p>
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>