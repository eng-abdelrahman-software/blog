@extends('admin.app')

@section('title', 'Article List')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-12">
                        @if($result['error'] && $result['code'] == 401)
                            <div class="alert alert-danger alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                                <span>{{$result['error']}}</span>
                            </div>
                        @endif
                        @if(!$result['error'] && $result['code'] == 401)
                            <div class="alert alert-success alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h5><i class="icon fas fa-check"></i> Alert!</h5>
                                your request has been done successfuly!
                            </div>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <h1 class="m-0">Articles</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard v1</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">


                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">@lang('admin.articles_table')</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>@lang('admin.title')</th>
                                            <th>@lang('admin.bio')</th>
                                            <th>@lang('admin.text')</th>
                                            <th>@lang('admin.user')</th>
                                            <th>@lang('admin.created_at')</th>
                                            <th>@lang('admin.edit')</th>
                                            <th>@lang('admin.delete')</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($result['data'])
                                            @foreach($result['data'] as $item)
                                            <tr>
                                                <td>{{$item->title}}</td>
                                                <td>{{Str::limit($item->bio, 20)}}</td>
                                                <td>{{Str::limit($item->text, 20)}}</td>
                                                <td>{{$item->username}}</td>
                                                <td>{{$item->created_at->format('Y-m-d')}}</td>
                                                <td>
                                                    @if(Auth::user()->id == $item->user_id)
                                                        <a href="{{route('admin_article.edit', $item->id)}}"><button type="button" id="editArticleBtn" class="btn btn-block btn-success" >@lang('admin.edit')</button></a>
                                                    @else
                                                        <p style="color: red">you are not the owner</p>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(Auth::user()->id == $item->user_id)
                                                        <a onclick="DeleteItem('{{ url('admin_article/'. $item->id ) }}')" title="delete" href="javascript:;" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                                                    @else
                                                        <p style="color: red">you are not the owner</p>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        @endif

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th>@lang('admin.title')</th>
                                            <th>@lang('admin.bio')</th>
                                            <th>@lang('admin.text')</th>
                                            <th>@lang('admin.user')</th>
                                            <th>@lang('admin.created_at')</th>
                                            <th>@lang('admin.edit')</th>
                                            <th>@lang('admin.delete')</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
            $( document ).ready(function() {
                $('#sidebar *').removeClass('active');
                $('#list a').addClass('active');
                });

                function DeleteItem(url) {
                    Swal.fire({
                        title:"Are you sure?",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Delete',
                        cancelButtonText: 'Cancel',
                        }).then((willDelete) => {
                        if (willDelete.value) {
                            console.log(url);
                            $.ajax({
                                type: "DELETE",
                                url: url,
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                success: function (res) {
                                    location.reload();
                                },
                                error: function (data, response) {
                                    location.reload();

                                },
                            });
                        }
                        });
                    }
            
    </script>
@endsection