@extends('admin.app')

@section('title', 'Edit Article')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">@lang('admin.edit_article')</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
<?php //dd($result); ?>
    <!-- Main content -->
    <section class="content">

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                                @lang('admin.edit_article')
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form id="editArticleForm" method="post" action="{{ url('/admin_article/' . $result['data']['id']) }}" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label>@lang('admin.title')</label>
                                    <input type="text" name="title" class="form-control" placeholder="@lang('admin.enter')" value="{{$result['data']['title']}}">
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.images')</label>
                                    <input type="file" name="my_file[]" required multiple>
                                </div>
                                <div class="form-group">
                                    <label>Bio</label>
                                    <textarea class="form-control" name="bio" rows="3" placeholder="@lang('admin.enter')">
                                        <?php echo $result['data']['bio']; ?>
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label>@lang('admin.text')</label>
                                      <textarea id="summernote" >
                                            <?php echo $result['data']['text']; ?>
                                      </textarea>
                                    <input type="text" name="text" id="text" class="form-control" placeholder="@lang('admin.enter')" hidden>
                                </div>
                                <button type="submit" id="editArticleBtn" class="btn btn-block btn-primary" style="width: 10%; float: right">@lang('admin.submit')</button>
                            </form>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h3 class="card-title">
                                CodeMirror
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
              <textarea id="codeMirrorDemo" class="p-3">
              </textarea>
                            <button type="button" id="convertBtn" class="btn btn-block btn-primary" style="width: 10%; float: right">Convert</button>
                        </div>
                        <div class="card-footer">
                        </div>
                    </div>
                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </section>
</div>
@endsection
@section('js')
    <script>
        $( document ).ready(function() {
            $('#sidebar *').removeClass('active');
            $('#add a').addClass('active');

            document.querySelector('.CodeMirror').CodeMirror.setValue($('#summernote').summernote('code'));

            $('#editArticleBtn').click(function (e) {
                e.preventDefault();
                console.log($('#summernote').summernote('code'));
                $('#text').val($('#summernote').summernote('code'));
                if($('#summernote').summernote('code') === ''){
                    $('#summernote').summernote('code', '<p></p>');
                }
                $('#editArticleForm').submit();
                //document.querySelector('.CodeMirror').CodeMirror.setValue($('#summernote').summernote('code'));
            });
            $('#convertBtn').click(function () {
                document.querySelector('.CodeMirror').CodeMirror.setValue($('#summernote').summernote('code'));
            });
            //validation
            $('#editArticleForm').validate({
                rules: {
                    title: {
                        required: true
                    },
                    my_file: {
                        required: true
                    },
                    bio: {
                        required: true
                    },
                },
                messages: {
                    title: {
                        required: "Title is required"
                    },
                    my_file: {
                        required: "Images are required"
                    },
                    bio: {
                        required: "Bio is required"
                    },
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>
@endsection

