<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login', function () {
    return view('auth/login');
})->name('login');

Route::resource('article', 'App\Http\Controllers\User\ArticleController');
Route::get('/', function () {
    return redirect('/article');
});

// Admin Auth  
Route::group(['middleware'=>'auth'],function () {
    Route::resource('admin_article', 'App\Http\Controllers\Admin\ArticleController');
    Route::get('/admin', function () {
        return redirect('/admin_article');
    });

});  




Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/loginController', [App\Http\Controllers\AuthController::class, 'login'])->name('loginController');
Route::get('/logout', [App\Http\Controllers\AuthController::class, 'logout'])->name('logout');