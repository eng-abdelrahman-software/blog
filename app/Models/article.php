<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class article extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'bio',
        'text',
        'user_id',
        'created_at',
        'updated_at',
    ];

    public function User()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function images()
    {
        return $this->hasMany('App\Models\image');
    }
}
