<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class image extends Model
{
    use HasFactory;

    public function Article()
    {
        return $this->belongsTo('App\Models\article', 'article_id');
    }
}
