<?php
/**
 * Created by PhpStorm.
 * User: U12740
 * Date: 2/19/2021
 * Time: 4:21 PM
 */

namespace App\Http\Services;

use App\Http\Repositories\ArticleRepository;
use App\Http\Repositories\ImageRepository;
use App\Http\Repositories\UserRepository;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Exceptions\CustomException;

class ArticleService{


    protected $articleRepository;
    protected $imageRepository;
    protected $userRepository;

    public function __construct(ArticleRepository $articleRepository, ImageRepository $imageRepository, UserRepository $userRepository)
    {
        $this->articleRepository = $articleRepository;
        $this->imageRepository = $imageRepository;
        $this->userRepository = $userRepository;

    }

    public function add($data){


        $validator = Validator::make($data, [
            'title'=>'required',
            'bio'=>'required',
            'text'=>'required',
            'my_file'=>'required'
        ]);

        if ($validator->fails()) {
//            $error = $validator->messages()->first();
            return null;
        }

        DB::beginTransaction();
        $result = $this->articleRepository->save($data);

        try {
            ini_set('memory_limit', '-1');
            $images = $data['my_file'];
            $savedImages = [];
            foreach ($images as $image) {
                if (!empty($image) && $image->isValid()) {
                    if ($image->isValid()) {

                        $filename = time() . '.' . $image->getClientOriginalExtension();
                        $filename_tosave = $filename;


                        $percent = 0.9;
                        list($width, $height) = getimagesize($image);

                        $big_width = $width * 0.9;
                        $big_height = $height * 0.9;

                        $medium_width = $width * 0.75;
                        $medium_height = $height * 0.75;

                        $small_width = $width * 0.5;
                        $small_height = $height * 0.5;

                        // Load
                        $big_thumb = imagecreatetruecolor($big_width, $big_height);
                        $medium_thumb = imagecreatetruecolor($medium_width, $medium_height);
                        $small_thumb = imagecreatetruecolor($small_width, $small_height);

                        $big_source = imagecreatefromjpeg($image);
                        $medium_source = imagecreatefromjpeg($image);
                        $small_source = imagecreatefromjpeg($image);

                        // Resize
                        imagecopyresized($big_thumb, $big_source, 0, 0, 0, 0, $big_width, $big_height, $width, $height);
                        imagecopyresized($medium_thumb, $medium_source, 0, 0, 0, 0, $medium_width, $medium_height, $width, $height);
                        imagecopyresized($small_thumb, $small_source, 0, 0, 0, 0, $small_width, $small_height, $width, $height);

                        imagejpeg($big_thumb, public_path('images/big_size/') . $filename);
                        imagejpeg($medium_thumb, public_path('images/medium_size/') . $filename);
                        imagejpeg($small_thumb, public_path('images/small_size/') . $filename);

                        $path = public_path('images/full_size');
                        $image->move($path, $filename);

                        $data = [
                            'name' => $filename,
                            'article_id' => $result->id
                        ];

                        $imgResult = $this->imageRepository->save($data);
                        if($imgResult) {
                            array_push($savedImages, $imgResult->name);
                        }

                    }

                }
            }

        } catch (Exception $e) {
            DB::rollBack();
            return null;
        }

        $result->savedImages = $savedImages;
        DB::commit();
        return $result;
    }

    public function update($data, $id){

        $validator = Validator::make($data, [
            'title'=>'required',
            'bio'=>'required',
            'text'=>'required',
//            'my_file'=>'required'
        ]);

        if ($validator->fails()) {
//            $error = $validator->messages()->first();
            return null;
        }


        DB::beginTransaction();
        try {
            $result = $this->articleRepository->update($data, $id);
        }catch (Exception $e){
            DB::rollBack();
            return null;
        }
        DB::commit();

        return $result;
    }

    public function getAll(){
        $articles = $this->articleRepository->getAll();
        foreach ($articles as $index => $item) {
            $user = $this->userRepository->getUser($item->user_id);
            $articles[$index]->username = $user->name;
            $articles[$index]->images = $this->imageRepository->getAll($item->id);
        }
        return $articles;
    }

    public function getById($id){
        $article = $this->articleRepository->getById($id);
        if($article) {
            $user = $this->userRepository->getUser($article->user_id);
            $article->username = $user->name;
            $article->images = $this->imageRepository->getAll($article->id);
        }
        return $article;
    }

    public function deleteArticle($id){

        DB::beginTransaction();
        try {
            $images = $this->imageRepository->delete($id);
            $article = $this->articleRepository->delete($id);
        }catch (Exception $e){
            DB::rollBack();
            return null;
        }
        DB::commit();

        return $article;
    }

}