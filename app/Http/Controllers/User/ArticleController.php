<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Services\ArticleService;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{

    protected $articleService;
    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
        //$this->middleware('ApiAuth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $articles = $this->articleService->getAll();
            $result = [
                'code' => 200,
                'data' => $articles,
                'error' => null
            ];
            return view('article/articlesList', compact('result'));
        } catch(Exception $e){
            $result = [
                'code' => 401,
                'data' => null,
                'error' => $e->getMessage()
            ];
            return view('article/articlesList', compact('result'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            $article = $this->articleService->getById($id);
            if($article) {
                $result = [
                    'code' => 200,
                    'data' => $article,
                    'error' => null
                ];
                return view('article/article', compact('result'));
            }else{
                $articles = $this->articleService->getAll();
                $result = [
                    'code' => 200,
                    'data' => $articles,
                    'error' => null
                ];
                return view('article/articlesList', compact('result'));
            }
        } catch(Exception $e){
            $result = [
                'code' => 401,
                'data' => null,
                'error' => $e->getMessage()
            ];
            return view('article/article', compact('result'));
        }
        return view('article/article');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
