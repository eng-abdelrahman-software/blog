<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Hash;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Services\AuthService;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseController
{
    protected $authService;
    public function __construct(AuthService $authService)
    { 
        $this->authService = $authService;
    }

    public function login(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email'=>'required',
            'password'=>'required'
        ]);
        
        if ($validator->fails()) {
            return view('auth.login',[
                'msg'=> "email or password is required"
            ]);
        }

        if (auth()->attempt(['email' => $request->email, 'password' => $request->password],1)) {
        return redirect('/admin');
        } else {
            return view('auth.login',[
                'msg'=> "Incorrect Email or Password"
            ]);
        }

    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
      }
}
