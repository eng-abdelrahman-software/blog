<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Services\ArticleService;

use App\Http\Controllers\Controller;

class ArticleController extends Controller
{

    protected $articleService;
    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
        //$this->middleware('ApiAuth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $articles = $this->articleService->getAll();
            $result = [
                'code' => 200,
                'data' => $articles,
                'error' => null
            ];
            return view('admin/articleList', compact('result'));
        } catch(Exception $e){
            $result = [
                'code' => 401,
                'data' => null,
                'error' => $e->getMessage()
            ];
            return view('admin/articleList', compact('result'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/addArticle');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only([
            'title',
            'bio',
            'text',
            'my_file'
        ]);

        $error = null;
        try{
            $article = $this->articleService->add($data);
            if(!$article) {
                $error = 'something went wrong';
            }
        } catch(Exception $e){
            $error = $e->getMessage();
        }

        // return to article List
        try{
            $articles = $this->articleService->getAll();
            $result = [
                'code' => 401,
                'data' => $articles,
                'error' => $error
            ];
            return view('admin/articleList', compact('result'));
        } catch(Exception $e){
            $result = [
                'code' => 402,
                'data' => null,
                'error' => $e->getMessage()
            ];
            return view('admin/articleList', compact('result'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $article = $this->articleService->getById($id);
            if($article->user_id == Auth::user()->id) {
                $result = [
                    'code' => 200,
                    'data' => $article,
                    'error' => null
                ];
                return view('admin/editArticle', compact('result'));
            }else{
                $error = 'you are not the owner!';
            }
        } catch(Exception $e){
            $error = $e->getMessage();
        }

        $articles = $this->articleService->getAll();

        $result = [
            'code' => 401,
            'data' => $articles,
            'error' => $error
        ];

        return view('admin/articleList', compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only([
            'title',
            'bio',
            'text',
//            'my_file'
        ]);

        $error = null;
        try{
            $article = $this->articleService->getById($id);
            if($article->user_id == Auth::user()->id) {
                $article = $this->articleService->update($data, $id);
                if (!$article) {
                    $error = 'something went wrong';
                }
            }else{
                $error = 'you are not the owner!';
            }
        } catch(Exception $e){
            $error = $e->getMessage();
        }

        // return to article List
        try{
            $articles = $this->articleService->getAll();
            $result = [
                'code' => 401,
                'data' => $articles,
                'error' => $error
            ];
            return view('admin/articleList', compact('result'));
        } catch(Exception $e){
            $result = [
                'code' => 402,
                'data' => null,
                'error' => $e->getMessage()
            ];
            return view('admin/articleList', compact('result'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $article = $this->articleService->getById($id);
            if($article->user_id == Auth::user()->id) {
                $articles = $this->articleService->deleteArticle($id);
                $result = [
                    'code' => 200,
                    'data' => null,
                    'error' => null
                ];
                return view('admin/articleList', compact('result'));
            }else{
                $result = [
                    'code' => 200,
                    'data' => null,
                    'error' => 'you are not the owner!'
                ];
                return view('admin/articleList', compact('result'));
            }
        } catch(Exception $e){
            $result = [
                'code' => 401,
                'data' => null,
                'error' => $e->getMessage()
            ];
            return view('admin/articleList', compact('result'));
        }
    }
}
