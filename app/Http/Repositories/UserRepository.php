<?php
/**
 * Created by PhpStorm.
 * User: U12740
 * Date: 2/19/2021
 * Time: 4:21 PM
 */

namespace App\Http\Repositories;

use App\Models\User;


class UserRepository{


    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }


    public function getUser($user_id){
        $user = $this->user->find($user_id);
        return $user;
    }


}