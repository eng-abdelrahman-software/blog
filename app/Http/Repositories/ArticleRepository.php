<?php
/**
 * Created by PhpStorm.
 * User: U12740
 * Date: 2/19/2021
 * Time: 4:21 PM
 */

namespace App\Http\Repositories;

use App\Models\article;
use Illuminate\Support\Facades\Auth;


class ArticleRepository{


    protected $article;

    public function __construct(article $article)
    {
        $this->article = $article;
    }

    public function save($data){

        $article = new $this->article;

        $article->title = $data['title'];
        $article->bio = $data['bio'];
        $article->text = $data['text'];
        $article->user_id = Auth::user()->id;

        $article->save();

        return $article;
    }

    public function update($data, $id){

        $article = $this->article->find($id);

        $article->title = $data['title'];
        $article->bio = $data['bio'];
        $article->text = $data['text'];

        $article->update();

        return $article;
    }

    public function getAll(){
        $articles = $this->article->orderBy('created_at', 'desc')->get();

        return $articles;
    }

    public function getById($id){
        $articles = $this->article->find($id);
        return $articles;
    }

    public function delete($id){
        $article = $this->article->where('id' , $id)->delete();
        return $article;
    }

}