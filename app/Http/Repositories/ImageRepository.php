<?php
/**
 * Created by PhpStorm.
 * User: U12740
 * Date: 2/19/2021
 * Time: 4:21 PM
 */

namespace App\Http\Repositories;

use App\Models\image;


class ImageRepository{


    protected $image;

    public function __construct(image $image)
    {
        $this->image = $image;
    }

    public function save($data){

        $image = new $this->image;

        $image->name = $data['name'];
        $image->article_id = $data['article_id'];

        $image->save();

        return $image;
    }

    public function getAll($article_id){
        $images = $this->image->where('article_id', $article_id)->get();
        return $images;
    }

    public function delete($id){
        $images = image::where('article_id' , $id)->delete();
        return $images;
    }


}