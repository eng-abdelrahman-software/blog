


## About Laravel

`php:8.0` `laravel:8.12`


Laravel project near to medium website with these feature :

- Admin panel to post/edit/delete/list articles
- Ability to add one or more photos to the article
- Ability to add any thing like (images, h1, h2, p, ....) to article 
- Frontend to list and display article
- Database migrations
- Database seeds

## Installation 

open a new directory and write the following commends

```
git clone https://gitlab.com/abd.r.horani/blog-talentlist.git
```

```
cd blog-talentlist
```

```
cp .env.example .env
```
create database and name it 'talentListdb'

```
composer install
```

```
php artisan migrate
```

```
php artisan db:seed
```
wait 10 seconeds please and then 

```
php artisan key:generate
```
```
php artisan serve
```
then you can browse the application : on 

[localhost:8000/](localhost:8000/)

you can use one of generated user by the seeds to login to admin panel

note : that all generated users by seeds have the same password : '**password**'

[localhost:8000/admin/](localhost:8000/admin/)
 
 appriciate your time
   
## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
