<?php

namespace Database\Factories;

use App\Models\image;
use App\Models\article;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = image::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $images = ['1613780452.jpeg', '1613780453.jpeg', '1613780483.jpeg', '1613811566.jpg',
            '1613811615.jpg', '1613812445.jpeg', '1613812446.jpeg', '1613812494.jpg', '1613812527.jpg', '1613812598.jpg'];
        $article = article::all()->pluck('id')->toArray();
        return [
            'article_id'=> $this->faker->randomElement($article),
            'name'=> $this->faker->randomElement($images)
        ];
    }
}
